# include <GL/glew.h>
# include <GL/freeglut.h>
# include <iostream>
# include <memory>
# include <vector>

# include "object_vbo.hh" // vector of points

# include "data.hh"
# include "particles.hh"
# include "program.hh"
# include "texture.hh"
# include "utils.hh"


std::shared_ptr<Data> background;
std::shared_ptr<Texture> texture_bg;

std::shared_ptr<Program> program_bg;
std::shared_ptr<Program> program_particle;
std::shared_ptr<ParticleManager> pm;


void window_resize(int width, int height)
{
    glViewport(0, 0, width, height);
    test_opengl_error(__FILE__, __LINE__);
}

void GLAPIENTRY
MessageCallback(GLenum source,
                GLenum type,
                GLuint id,
                GLenum severity,
                GLsizei length,
                const GLchar *message,
                const void *userParam)
{
    fprintf(stderr, "GL CALLBACK: %s type = 0x%x, severity = 0x%x, message = %s\n",
            (type == GL_DEBUG_TYPE_ERROR ? "** GL ERROR **" : ""),
            type, severity, message);
}

void update()
{
    // update the particles
    pm->update();

    // to call display again
    glutPostRedisplay();
}

void display()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // order: foreground -> background

    // display particles
    pm->display();

    // use texture
    glBindTexture(GL_TEXTURE_2D, texture_bg->tex_id);
    // display background
    background->display();

    glFinish();
    glutSwapBuffers();
    test_opengl_error(__FILE__, __LINE__);
}

void initGlut(int &argc, char *argv[])
{
    glutInit(&argc, argv);
    glutInitContextVersion(4, 5); // OpenGL version 4.5
    glutInitContextProfile(GLUT_CORE_PROFILE | GLUT_DEBUG);
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
    glutInitWindowSize(512, 512); // window size
    glutInitWindowPosition(100, 100); // window position
    glutCreateWindow("Our Fire"); // title of window
    // display
    glutDisplayFunc(display);
    // update and call display again
    glutIdleFunc(update);
    glutReshapeFunc(window_resize);
}

bool initGlew()
{
    if (glewInit()) {
        std::cerr << " Error while initializing glew";
        return false;
    }
    return true;
}

void initGL()
{
    glEnable(GL_DEPTH_TEST);
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    glEnable(GL_CULL_FACE);

    // set color to grey
    glClearColor(0.3, 0.3, 0.3, 1.0);

    // set alignment requirements for the start of each pixel row in memory
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    glPixelStorei(GL_PACK_ALIGNMENT, 1);

    test_opengl_error(__FILE__, __LINE__);
}

void init_data()
{
    // background
    std::vector<std::string> fields_bg = {
        "position",
        "texCoord",
    };
    background = std::make_shared<Data>(program_bg->program_id,
                                        fields_bg, datas_background);

    // particles
    std::array<float, 2> origin = {80, 60};
    size_t nb_particles = 2000;
    pm = std::make_shared<ParticleManager>(
        origin,
        nb_particles,
        0.01,
        15,
        program_particle->program_id
    );
}

int main(int argc, char *argv[])
{
    // Glut init
    initGlut(argc, argv);
    std::cerr << "Glut init Ok\n";

    // Glew init
    initGlew();
    std::cerr << "Glew init Ok\n";

    // GL init
    initGL();
    glEnable(GL_DEBUG_OUTPUT);
    glDebugMessageCallback(MessageCallback, 0);
    std::cerr << "GL init Ok\n";

    // init program shaders
    program_bg = std::make_shared<Program>("../shaders/vertex_background.shd",
                                           "../shaders/fragment_background.shd");
    program_particle = std::make_shared<Program>("../shaders/vertex_particle.shd",
                                                 "../shaders/fragment_particle.shd");
    std::cerr << "init shaders Ok\n";

    // init data
    init_data();
    std::cerr << "init data Ok\n";

    // init texture
    texture_bg = std::make_shared<Texture>("../textures/background.png");
    std::cerr << "init texture Ok\n";

    std::cerr << "Start\n";

    // glut event loop
    glutMainLoop();
    return 0;
}
