// Vertex List

static const std::vector<std::vector<GLfloat>> datas_background = {
    // vertex_buffer
    {
         // good
        -1, -1, 0,
         1, -1, 0,
        -1,  1, 0,
        //
        -1,  1, 0,
         0,  0, 0,
         1,  1, 0,
        //
         1,  0, 0,
         0,  0, 0,
         1, -1, 0,
        //
         1,  1, 0,
         0,  0, 0,
         1,  0, 0,
    },
    // texture coords
    {
        // good
          0, 1, 0,
          1, 1, 0,
          0, 0, 0,

          0,   0, 0,
        0.5, 0.5, 0,
          1,   0, 0,
        //
          1, 0.5, 0,
        0.5, 0.5, 0,
          1,   1, 0,
        //
          1,   0, 0,
        0.5, 0.5, 0,
          1, 0.5, 0,
    }
};
