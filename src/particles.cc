//
// Created by hiiro on 6/2/20.
//

# include <algorithm>
# include <cmath>
# include <ctime>
# include <cstdlib>
# include <iostream>

# include "particles.hh"
# include "utils.hh"

Particle::Particle(std::array<float, 2> origin)
{
    this->init(origin);
}

void Particle::init(std::array<float, 2> origin)
{
    // between 0 and 1
    float rand_pos_x = rand() % 100 / 100.0;
    float rand_pos_y = rand() % 100 / 100.0;
    // small direction
    float rand_dir_x = (rand() % 100 / 500.0) - 0.1;
    float rand_dir_y = rand() % 100 / 500.0;

    this->pos = {origin[0] + rand_pos_x, origin[1] + rand_pos_y};
    this->dir = {rand_dir_x, rand_dir_y};
    this->rgba = {0.75, 0.75, 0.1, 1.0}; // yellow
    this->lifetime = 1;
}

void Particle::update(float timelapse)
{
    this->lifetime -= timelapse / ((this->dir[1] + 0.5) * 2);

    if (this->lifetime > 0.f) {
        /* Move particle */
        this->pos[0] += this->dir[0];
        this->pos[1] += this->dir[1];

        /* Make it less bright */
        this->rgba[1] -= timelapse * (this->dir[1] + fabs(this->dir[0]) * 0.5) * 3;

        if (this->lifetime < 0.5f) {
            // yellow to red
            this->rgba[0] -= timelapse * (this->dir[1] + fabs(this->dir[0]) * 0.5);
        }
    }
}

ParticleManager::ParticleManager(std::array<float, 2> origin,
                                 int nb_particles,
                                 float dt,
                                 int nb_regen,
                                 int program_id)
        : origin{origin}
        , nb_particles{nb_particles}
        , dt{dt}
        , nb_regen{nb_regen}
        , program_id(program_id)
{
    GLuint vbo_id;
    GLfloat particle_triangles[] = {
            -0.5, -0.5, 0,
             0,   -0.5, 0,
             0,    0.5, 0,

             0,    0.5, 0,
             0,   -0.5, 0,
             0.5, -0.5, 0,
    };

    // object id
    glGenVertexArrays(1, &vba_id);
    glBindVertexArray(vba_id);
    test_opengl_error(__FILE__, __LINE__);

    // id for position var
    glGenBuffers(1, &vbo_id);
    test_opengl_error(__FILE__, __LINE__);

    // set buffer datas
    glBindBuffer(GL_ARRAY_BUFFER, vbo_id);
    glBufferData(GL_ARRAY_BUFFER, 6 * 3 * sizeof(GLfloat), particle_triangles, GL_STATIC_DRAW);
    test_opengl_error(__FILE__, __LINE__);

    // position = 3 float
    GLint loc = glGetAttribLocation(program_id, "position");
    glVertexAttribPointer(loc, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
    glEnableVertexAttribArray(loc);
    test_opengl_error(__FILE__, __LINE__);

    glBindVertexArray(0);

    srand(time(nullptr));
    for (auto i = 0; i < nb_particles / 50; i++)
        particles.push_back(Particle(origin));
}

int ParticleManager::find_dead_particle() const
{
    for (auto i = 0; i < this->particles.size(); i++) {
        if (this->particles[i].lifetime <= 0.f)
            return i;
    }
    return 0;
}

void ParticleManager::update()
{
    // move particles and change color
    for (auto &p : this->particles)
        p.update(dt);

    // recreate some dead particles
    for (auto i = 0; i < this->nb_regen; i++) {
        auto dead_idx = find_dead_particle();
        // move at init pos with init color
        this->particles[dead_idx].init(this->origin);
    }

    // add remaining particles
    int to_add_particles = this->nb_particles - this->particles.size();
    for (auto i = 0; i < std::min(to_add_particles, 15); ++i)
        this->particles.push_back(Particle(this->origin));
}

void ParticleManager::display() const
{
    glUseProgram(this->program_id);

    for (Particle p : this->particles)
    {
        if (p.lifetime <= 0.f)
            continue;

        // set movement of the particle
        auto off_loc = glGetUniformLocation(this->program_id, "off");
        glUniform2f(off_loc, p.pos[0], p.pos[1]);
        test_opengl_error(__FILE__, __LINE__);

        // set color of the particle
        auto color_loc = glGetUniformLocation(this->program_id, "color");
        glUniform4f(color_loc, p.rgba[0], p.rgba[1], p.rgba[2], p.rgba[3]);
        test_opengl_error(__FILE__, __LINE__);

        // display the particle
        glBindVertexArray(vba_id);
        glDrawArrays(GL_TRIANGLES, 0, 6 * 3); // 2 triangles
        test_opengl_error(__FILE__, __LINE__);
        glBindVertexArray(0);
    }
}
