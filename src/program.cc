# include <iostream>

# include "program.hh"

Program::Program(const std::string &vertex_filename, const std::string &frag_filename)
    : vertex(vertex_filename, GL_VERTEX_SHADER)
    , fragment(frag_filename, GL_FRAGMENT_SHADER)
{
    // create shaders
    if (!this->vertex.create() || !this->fragment.create())
        goto error;

    // create the program object
    this->program_id = glCreateProgram();

    // error checking
    if (this->program_id == 0)
    {
        std::cerr << "Can't init program" << std::endl;
        goto error;
    }

    // link
    if (this->vertex.link(this->program_id) && this->fragment.link(this->program_id))
        return; // success

    // delete all
    glDeleteProgram(this->program_id);

error:
    std::cerr << "Error in creating program" << std::endl;
    this->vertex.delete_shader();
    this->fragment.delete_shader();
    this->program_id = -1;
}
