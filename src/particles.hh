#pragma once

# include <GL/glew.h>

# include <array>
# include <vector>
# include "shader.hh"

class Particle {
public:

    Particle(std::array<float, 2> origin);

    void init(std::array<float, 2> origin);
    void update(float timelapse);

    float lifetime;
    std::array<float, 2> pos;
    std::array<float, 2> dir;
    std::array<float, 4> rgba;
};

class ParticleManager {
public:
    ParticleManager(std::array<float, 2> origin,
                    int nb_particles,
                    float dt,
                    int nb_regen,
                    GLint program_id);

    int find_dead_particle() const;
    void update();
    void display() const;

private:
    std::vector<Particle> particles;

    std::array<float, 2> origin;
    int nb_particles;
    float dt;
    int nb_regen;

    GLuint vba_id;
    GLint program_id;
};
