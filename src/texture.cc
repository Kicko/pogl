# include <GL/glew.h>
# include <SOIL/SOIL.h>
# include <iostream>

# include "texture.hh"
# include "utils.hh"

Texture::Texture(const char *filename)
{
    glGenTextures(1, &this->tex_id);
    glBindTexture(GL_TEXTURE_2D, this->tex_id);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    int width, height;
    // get image
    unsigned char *image = SOIL_load_image(filename, &width, &height, 0, SOIL_LOAD_RGB);
    if (!image) {
        std::cout << "Can't load texture: " << SOIL_last_result() << std::endl;
        return;
    }

    // generate texture
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
    // free image
    SOIL_free_image_data(image);
}
