# pragma once

# include <string>

// load shader from file
std::string load(const std::string &filename);

// print type and line of error
void test_opengl_error(const char *file, int line);
