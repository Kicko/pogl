# pragma once

# include <GL/glew.h>
# include "shader.hh"

class Program
{
public:
    Program(const std::string &vertex_filename, const std::string &frag_filename);

    GLint program_id;
private:
    Shader vertex;
    Shader fragment;
};
